//
//  ViewController.swift
//  DancingGirlAR
//
//  Created by Cemal Bayrı on 28.12.2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import Firebase

class ViewController: UIViewController,ARSCNViewDelegate, getProtocol {
    
    @IBOutlet var sceneView: ARSCNView!
    
    var animations = [String: CAAnimation]()
    var idle:Bool = true
    var planePosition: SCNVector3! = SCNVector3(x:0,y:0,z:-1)
    var myNodes = [SCNNode]()
    var myPlaneAnchor: ARPlaneAnchor?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        FBProcess.getDelegate = self
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
//        setConfiguration()
        
//        DispatchQueue.main.async {
//            FBProcess.get()
//        }
        

        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        setConfiguration()
    }
    
    func load(_ withPos: SCNVector3) {
        let scene = wearUpMadaFucker()
        let node = SCNNode()
        let nodeArray = scene.rootNode.childNodes
        for childNode in nodeArray {
            childNode.position = withPos
            node.scale = SCNVector3(x:0.01, y: 0.01, z: 0.01)
            node.addChildNode(childNode as SCNNode)
        }
        sceneView.scene.rootNode.addChildNode(node)
    }
    
    func setConfiguration() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.delegate = self
        sceneView.session.run(configuration)
    }
    
    func tryGet(data: [DocumentSnapshot]) {
        print(data)
        DispatchQueue.main.async {
            self.setConfiguration()
            if data.count != 0 {
                for d in data {
                    let d = d.data()
                    let position = SCNVector3(d["lat"] as! CGFloat ,d["long"] as! CGFloat, 0 )
                    print(position)
                    self.load(position)
                }
               
            }
        }
    }
    
    func createObject(position: SCNVector3) -> SCNNode {
        
        let node =  SCNNode()
//        let scene = SCNScene(named: "art.scnassets/dancefixed.dae")
        let scene = wearUpMadaFucker()
        let nodeArray = scene.rootNode.childNodes
        for childNode in nodeArray {
            node.addChildNode(childNode as SCNNode)
        }
        sceneView.scene.rootNode.addChildNode(node)
  
        
        node.name = "dance"
        node.position = position
        myNodes.append(node)
        
        return node
    }
    
 
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let location = touch?.location(in: sceneView)
        
        if(touch?.view == self.sceneView){
            print("touch working")
            let viewTouchLocation:CGPoint = touch!.location(in: sceneView)
            guard let result = sceneView.hitTest(viewTouchLocation, options: nil).first else {
                return
            }
            
            if result.node.name == "plane" {
                print("plane e girdi")
                let hitResults = sceneView.hitTest(location!, types: .existingPlaneUsingExtent)
                
                if hitResults.count > 0 {
                    let result: ARHitTestResult = hitResults.first!
                    FBProcess.add(location: location!, completion: {
                        let newLocation = SCNVector3Make(result.worldTransform.columns.3.x, result.worldTransform.columns.3.y, result.worldTransform.columns.3.z)
                        let node = self.createObject(position: newLocation)
                    })
                }
                
            }
            else {
              
            }
            
        }
    }
    
    func wearUpMadaFucker() -> SCNScene {
        let scene = SCNScene(named: "art.scnassets/dancefixed.dae")!
        
        var nodeBody = SCNNode()
        var nodeSkin = SCNNode()
        var nodeFace = SCNNode()
        var nodeHead = SCNNode()
        
        nodeBody.name = "body"
        nodeSkin.name = "skin"
        nodeFace.name = "face"
        nodeHead.name = "head"
//
        let material = SCNMaterial()
        
        nodeBody = scene.rootNode.childNode(withName: "HotGirl01BodyCloth", recursively: true)!
        nodeSkin = scene.rootNode.childNode(withName: "HotGirl01BodySkin", recursively: true)!
        
        nodeBody.scale = SCNVector3(x:0.1, y: 0.1, z: 0.1)
        nodeSkin.scale = SCNVector3(x:0.1, y: 0.1, z: 0.1)
        
        material.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Body_D.tga")

        let material1 = SCNMaterial()
        material1.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Body_N.tga")

        let material2 = SCNMaterial()
        material2.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Body_S.tga")

        nodeBody.geometry?.materials = [material,material1,material2]
        nodeSkin.geometry?.materials = [material,material1,material2]

        nodeHead = scene.rootNode.childNode(withName: "HotGirl01Hair", recursively: true)!
        nodeFace = scene.rootNode.childNode(withName: "HotGirl01Head", recursively: true)!
        
        nodeHead.scale = SCNVector3(x:0.1, y: 0.1, z: 0.1)
        nodeFace.scale = SCNVector3(x:0.1, y: 0.1, z: 0.1)
        
        let material3 = SCNMaterial()
        material3.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Head_D.tga")
        
        let material4 = SCNMaterial()
        material4.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Head_N.tga")
        
        let material5 = SCNMaterial()
        material5.diffuse.contents = UIImage(named: "art.scnassets/Hotgirl01_Head_S.tga")
        
        nodeHead.geometry?.materials = [material3,material4,material5]
        nodeFace.geometry?.materials = [material3,material4,material5]

        return scene
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}

extension ViewController: ARSessionDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // Place content only for anchors found by plane detection.
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        // Create a SceneKit plane to visualize the plane anchor using its position and extent.
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        let planeNode = SCNNode(geometry: plane)
        planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
        planePosition = planeNode.position
        self.myPlaneAnchor = planeAnchor
        /*
         `SCNPlane` is vertically oriented in its local coordinate space, so
         rotate the plane to match the horizontal orientation of `ARPlaneAnchor`.
         */
        planeNode.eulerAngles.x = -.pi / 2
        planeNode.name = "plane"
        // Make the plane visualization semitransparent to clearly show real-world placement.
        planeNode.opacity = 0.25
        
        /*
         Add the plane visualization to the ARKit-managed node so that it tracks
         changes in the plane anchor as plane estimation continues.
         */
        node.addChildNode(planeNode)
        myNodes.append(planeNode)
    }
    
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // Update content only for plane anchors and nodes matching the setup created in `renderer(_:didAdd:for:)`.
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }
        
        // Plane estimation may shift the center of a plane relative to its anchor's transform.
        planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
        
        /*
         Plane estimation may extend the size of the plane, or combine previously detected
         planes into a larger one. In the latter case, `ARSCNView` automatically deletes the
         corresponding node for one plane, then calls this method to update the size of
         the remaining plane.
         */
        plane.width = CGFloat(planeAnchor.extent.x)
        plane.height = CGFloat(planeAnchor.extent.z)
    }
    
    
}
