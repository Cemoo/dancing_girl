//
//  FBProcess.swift
//  DancingGirlAR
//
//  Created by Cemal Bayrı on 29.12.2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation
import MapKit


protocol getProtocol {
    func tryGet(data: [DocumentSnapshot])
}

class FBProcess {
    static var ref: DocumentReference? = nil
    static var db = Firestore.firestore()
    static var getDelegate: getProtocol?
    static var map: MKMapView?
    
    static func add(location: CGPoint, completion: @escaping () ->()) {
        ref = db.collection("locations").addDocument(data: [
            "lat": self.convertToCL2d(location).latitude,
            "long": self.convertToCL2d(location).longitude,
            "alti": self.convertToCL2d(location).latitude
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                completion()
            }
        }
    }
    
    static func get() {
        db.collection("locations").getDocuments() { (querySnapshot, err) in
            var arr = [[String: Any]]()
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    arr.append(document.data())
                   
                }
                
                getDelegate?.tryGet(data: (querySnapshot?.documents)!)
                
            }
        }
    }
    
    static func convertToCL2d(_ point: CGPoint) -> CLLocationCoordinate2D {
        
        let newCoordinate = self.map?.convert(point, toCoordinateFrom:self.map)
        let annotation = MKPointAnnotation()
        annotation.coordinate = newCoordinate!
    
        return annotation.coordinate
    }
    
    
    
}

